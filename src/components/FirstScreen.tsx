import React, {  useEffect } from 'react';
import '../public/styles/firstScreen.scss';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAction } from '../hooks/useAction';

const FirstScreen: React.FC = () => {

  const { items, search,loader,id } = useTypedSelector(state => state._git);
  const { apiGitSearchFetch, apiGitRepoFetch, searchGitInput } = useAction();


  useEffect(() => {

    apiGitSearchFetch(search)


  }, [search]);



  function renderList(): React.ReactNode {
    if (items.length > 0) {
      return items.map(elem =>
        (

          <div key={elem.id}
               className='firstScr-list' style={{ display: 'flex', padding: 10 }}>
            <div style={{ width: 90, height: 90 }}>
              <img style={{ width: 90, height: 90 }} src={elem.avatar_url} alt="img"/>
            </div>

            <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>


              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginLeft: 20 }}>
                <a className='customBtn'  onClick={() => apiGitRepoFetch(elem.login,elem.id)}>{elem.login}</a>

                {(loader && id===elem.id) && <div className="spinnerBlock"><div className="spinner"></div></div>}

              </div>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginRight: 20 }}>
                <a className='customBtn' href={elem.html_url} target={'_blank'}>Repo</a>
              </div>

            </div>

          </div>
        ));

    }

  };

  return (
    <div className='firstScr'>
      <div className='firstScr-title'>
        First Screen
      </div>

      <div className='firstScr-block'>
        <div className='firstScr-searcher'>
          <div className="firstScr-searcher__title">
            GitHub Searcher
          </div>
          <div className="firstScr-searcher__wrap">
            <input
              onChange={e => {
                searchGitInput(e);
              }}
              className='firstScr-searcher__wrap-input'
              type="text" placeholder='Search for Users'/>
          </div>

        </div>

        <div className="firstSrc-show">
          {renderList()}
        </div>

      </div>
    </div>
  );
};

export default FirstScreen;