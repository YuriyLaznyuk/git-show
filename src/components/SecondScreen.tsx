import React, { useEffect } from 'react';
import '../public/styles/secondScreen.scss';
import { useTypedSelector } from '../hooks/useTypedSelector';
import { useAction } from '../hooks/useAction';

const SecondScreen: React.FC = () => {
  const { search, user, foundRepos, repos }
    = useTypedSelector(state => state._repo);
  const { searchEffect, searchInput } = useAction();

  useEffect(() => {

    searchEffect(search, repos);

  }, [search]);


  const renderRepos = (): React.ReactNode => {
    if (foundRepos.length > 0) {
      return foundRepos.map(repo => (
        <div className='secondScr-repo' key={repo.id}>
          <div className="repo-link">
            <a className='customBtn' target='_blank'
               href={repo.html_url}>{repo.name}</a>
          </div>
          <div className="repo-ifo">
            <div><strong>{repo.forks_count}</strong> Fork</div>
            <div><strong>{repo.stargazers_count}</strong> Stars</div>
          </div>
        </div>
      ));
    }
  };

  return (
    <div className='secondScr'>
      <div className='secondScr-title'>
        Second Screen
      </div>
      <div className="secondScr-section">
        <div className="secondScr-section__title">GitHubSearcher</div>
        <div className="secondScr-topBlock">
          <div className="secondScr-topBlock__avatar">
            <img className="secondScr-topBlock__avatar-img"
                 src={user.avatar_url} alt="avatar"/>
          </div>
          <div className="secondScr-topBlock__items">
            <div className="secondScr-topBlock__items-itm">UserName: <strong>{user.name}</strong></div>
            <div className="secondScr-topBlock__items-itm">Email: <strong>{user.email}</strong></div>
            <div className="secondScr-topBlock__items-itm">Location: <strong>{user.location}</strong></div>
            <div className="secondScr-topBlock__items-itm">Join Date: <strong>{user.created_at}</strong></div>
            <div className="secondScr-topBlock__items-itm">Followers: <strong>{user.followers}</strong></div>
            <div className="secondScr-topBlock__items-itm">Following: <strong>{user.following}</strong></div>

          </div>
        </div>
        <div className="secondScr-text">
          This is their biography. It may be long and needs to all fit.
        </div>

        <div className='secondScr-bottomBlock'>
          <div className='secondScr-searcher'>

            <div className="secondScr-searcher__wrap">
              <input
                onChange={e => searchInput(e)}
                value={search}
                className='secondScr-searcher__wrap-input'
                type="text" placeholder='Search for Users Repositories'/>
            </div>

          </div>
        </div>

        <div className="reposShow">
          {renderRepos()}

        </div>

      </div>


    </div>
  );
};

export default SecondScreen;