interface IPropertyUser {
  name: string,
  email: string | null,
  location: string | null,
  created_at: string,
  followers: number | string,
  following: number | string,
  avatar_url: string
}

export interface IRepoState {
  user: IPropertyUser,
  visible: boolean,
  repos: any[],
  search: string,
  foundRepos: any[]


}

export const defaultState: IRepoState = {
  user: {
    name: '',
    created_at: '',
    email: '',
    followers: '',
    following: '',
    location: '',
    avatar_url: ''
  },
  visible: false,
  repos: [],
  search: '',
  foundRepos: []
};

export enum RepoActionType {
  VISIBLE_REPO = 'VISIBLE_REPO',
  VISIBLE_CHANGE = 'VISIBLE_CHANGE',
  SEARCH_REPO = 'SEARCH_REPO',
  SEARCH_INPUT = 'SEARCH_INPUT'

}

interface SearchInputAction {
  type: RepoActionType.SEARCH_INPUT,
  payload: string
}

interface VisibleRepoAction {
  type: RepoActionType.VISIBLE_REPO,
  payload: {
    repos: any[],
    user: IPropertyUser,

  }
}

interface VisibleChangeAction {
  type: RepoActionType.VISIBLE_CHANGE,
  payload: boolean
}

interface SearchRepoAction {
  type: RepoActionType.SEARCH_REPO,
  payload: any[]
}

export type RepoAction = VisibleRepoAction | SearchRepoAction
  | VisibleChangeAction | SearchInputAction