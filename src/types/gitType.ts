export enum GitActionType {
  SEARCH_GIT = 'SEARCH_GIT',
  FETCH_GIT = 'FETCH_GIT',
  DEFAULT_GIT = 'DEFAULT_GIT',
  LOADER_GIT = 'LOADER_GIT'
}

export interface IGitReducerState {
  search: string,
  items: any[];
  loader: boolean;
  id:number | null;
}

export const defaultGitState: IGitReducerState = {
  search: '',
  items: [],
  loader: false,
  id:null
};


interface LoaderGitAction {
  type: GitActionType.LOADER_GIT;
  payload: {
    loader:boolean,
    id:number | null
  };
}


interface SearchGitAction {
  type: GitActionType.SEARCH_GIT,
  payload: string

}

interface FetchGitAction {
  type: GitActionType.FETCH_GIT,
  payload: any
}

interface DefaultGitAction {
  type: GitActionType.DEFAULT_GIT
}

export type GitAction = SearchGitAction | FetchGitAction
  | DefaultGitAction | LoaderGitAction