import React from 'react';
import FirstScreen from '../components/FirstScreen';
import SecondScreen from '../components/SecondScreen';
import '../public/styles/main.scss';
import { useTypedSelector } from '../hooks/useTypedSelector';


const Main: React.FC = () => {
  const { visible } = useTypedSelector(state => state._repo);
  return (
    <div className='main'>
      <FirstScreen/>
      {visible && <SecondScreen/>}
    </div>
  );
};

export default Main;