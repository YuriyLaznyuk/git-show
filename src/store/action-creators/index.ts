import * as RepoActionCreators from './repo';
import *as GitActionCreators from './git';


export default {
  ...RepoActionCreators,
  ...GitActionCreators
};