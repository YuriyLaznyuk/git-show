import { AppDispatch } from '../index';
import { GitActionType } from '../../types/gitType';
import React from 'react';
import { RepoActionType } from '../../types/repoType';


export const apiGitSearchFetch = (search: string) =>
  async (dispatch: AppDispatch): Promise<void> => {


    try {
      if (search !== '') {

        const resp = await fetch(`https://api.github.com/search/users?q=${search}&per_page=5&page=1`);

        const json = await resp.json();
        dispatch({ type: GitActionType.FETCH_GIT, payload: json.items });
      }
      if (search === '') {
        dispatch({ type: GitActionType.DEFAULT_GIT });
      }

    } catch (err) {
      console.log(err);
    }


  };


export const searchGitInput = (e: React.ChangeEvent<HTMLInputElement>) =>
  async (dispatch: AppDispatch): Promise<void> => {
    dispatch({ type: GitActionType.SEARCH_GIT, payload: e.target.value });
  };


export const apiGitRepoFetch = (login: string, id: number | null) =>
  async (dispatch: AppDispatch): Promise<void> => {

    dispatch({ type: GitActionType.LOADER_GIT, payload: { loader: true, id: id } });
    const repo = await fetch(`https://api.github.com/users/${login}`);
    const repos = await fetch(`https://api.github.com/users/${login}/repos`);
    const _repo = await repo.json();
    const _repos = await repos.json();

    dispatch({ type: RepoActionType.VISIBLE_REPO, payload: { user: _repo, repos: _repos } });
    dispatch({ type: RepoActionType.SEARCH_REPO, payload: [] });
    dispatch({ type: RepoActionType.SEARCH_INPUT, payload: '' });
    dispatch({ type: GitActionType.LOADER_GIT, payload: { loader: false, id: null } });


  };

