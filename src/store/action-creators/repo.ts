import { RepoAction, RepoActionType } from '../../types/repoType';
import { Dispatch } from 'redux';
import React from 'react';

export const searchEffect =
   (search: string,repos: any[]) =>async (dispatch: Dispatch<RepoAction>)=>{

  if (search.length > 0) {
    const result: any[] = repos.filter(items => items.name.includes(search));
    dispatch({ type: RepoActionType.SEARCH_REPO, payload: result });
  }
  if (search === '') {
    dispatch({ type: RepoActionType.SEARCH_REPO, payload: [] });
  }


};


export const searchInput =
  (e: React.ChangeEvent<HTMLInputElement>) =>
    async (dispatch: Dispatch<RepoAction>)=>{
    dispatch({ type: RepoActionType.SEARCH_INPUT, payload: e.target.value });
  };