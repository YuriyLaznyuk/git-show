import { applyMiddleware, combineReducers, createStore } from 'redux';
import { gitReducer } from './reducers/gitReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { repoReducer } from './reducers/repoReducer';

export const rootReducer = combineReducers({
  _git: gitReducer,
  _repo:repoReducer
});

export const store = createStore(rootReducer,
  composeWithDevTools(applyMiddleware(thunk)));
export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch=typeof store.dispatch;