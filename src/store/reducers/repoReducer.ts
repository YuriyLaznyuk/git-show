import { defaultState, IRepoState, RepoAction, RepoActionType } from '../../types/repoType';

export function repoReducer(state = defaultState, action: RepoAction): IRepoState {
  switch (action.type) {

    case RepoActionType.VISIBLE_REPO:
      return {
        ...state, visible: true,
        repos: action.payload.repos,
        user: action.payload.user
      };

    case RepoActionType.SEARCH_REPO:
      return { ...state, foundRepos: action.payload };

    case RepoActionType.VISIBLE_CHANGE:
      return { ...state, visible: action.payload };

    case RepoActionType.SEARCH_INPUT:
      return { ...state, search: action.payload };

    default:
      return state;
  }
}