import { defaultGitState, GitAction, GitActionType, IGitReducerState } from '../../types/gitType';

export function gitReducer(state = defaultGitState, action: GitAction): IGitReducerState {
  switch (action.type) {
    case GitActionType.DEFAULT_GIT:
      return { ...state, ...defaultGitState };
    case GitActionType.FETCH_GIT:
      return { ...state, items: action.payload };
    case GitActionType.SEARCH_GIT:
      return { ...state, search: action.payload };
      case GitActionType.LOADER_GIT:
        return {...state, loader:action.payload.loader, id:action.payload.id};
    default:
      return state;
  }
}