import React from 'react';
import './app.scss';
import Main from './pages/Main';

const App :React.FC = () => {
  return (
    <div>
      <h1>Git-Show</h1>
      <Main/>
    </div>
  );
};

export default App;